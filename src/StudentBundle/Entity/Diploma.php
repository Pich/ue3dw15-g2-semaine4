<?php

namespace StudentBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use StudentBundle\Entity\Profile;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Diploma
 *
 * @ORM\Table(name="diploma")
 * @ORM\Entity(repositoryClass="StudentBundle\Repository\DiplomaRepository")
 */
class Diploma
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * One Diploma has Many Profiles.
     * @ORM\OneToMany(targetEntity="Profile", mappedBy="diploma")
     */
    private $profiles;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var bool
     *
     * @ORM\Column(name="is_online", type="boolean")
     */
    private $isOnline;

    public function __construct()
    {
        $this->profiles = new ArrayCollection();
    }


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Diploma
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set isOnline
     *
     * @param boolean $isOnline
     *
     * @return Diploma
     */
    public function setIsOnline($isOnline)
    {
        $this->isOnline = $isOnline;

        return $this;
    }

    /**
     * Get isOnline
     *
     * @return bool
     */
    public function getIsOnline()
    {
        return $this->isOnline;
    }

    /**
     * Add profile
     *
     * @param \StudentBundle\Entity\Profile $profile
     *
     * @return Diploma
     */
    public function addProfile(\StudentBundle\Entity\Profile $profile)
    {
        $this->profiles[] = $profile;

        return $this;
    }

    /**
     * Remove profile
     *
     * @param \StudentBundle\Entity\Profile $profile
     */
    public function removeProfile(\StudentBundle\Entity\Profile $profile)
    {
        $this->profiles->removeElement($profile);
    }

    /**
     * Get profiles
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getProfiles()
    {
        return $this->profiles;
    }
}
