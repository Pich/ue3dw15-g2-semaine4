<?php
// src/AppBundle/Entity/User.php

namespace StudentBundle\Entity;

use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;
use StudentBundle\Entity\Profile;

/**
 * @ORM\Entity
 * @ORM\Table(name="fos_user")
 */
class User extends BaseUser
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * One User has One Profile.
     * @ORM\OneToOne(targetEntity="Profile", mappedBy="user")
     */
    private $profile;

    public function __construct()
    {
        parent::__construct();
        // your own logic
    }

    /**
     * Set profile
     *
     * @param \StudentBundle\Entity\Profile $profile
     *
     * @return User
     */
    public function setProfile(\StudentBundle\Entity\Profile $profile = null)
    {
        $this->profile = $profile;

        return $this;
    }

    /**
     * Get profile
     *
     * @return \StudentBundle\Entity\Profile
     */
    public function getProfile()
    {
        return $this->profile;
    }
}
