<?php

namespace StudentBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use StudentBundle\Entity\User;
use StudentBundle\Entity\Diploma;

/**
 * Profile
 *
 * @ORM\Table(name="profile")
 * @ORM\Entity(repositoryClass="StudentBundle\Repository\ProfileRepository")
 */
class Profile
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * One Profile has One User.
     * @ORM\OneToOne(targetEntity="User", inversedBy="profile")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    private $user;

    /**
     * Many Profiles have One Diploma.
     * @ORM\ManyToOne(targetEntity="Diploma", inversedBy="profiles")
     * @ORM\JoinColumn(name="diploma_id", referencedColumnName="id")
     */
    private $diploma;

    /**
     * @var string
     *
     * @ORM\Column(name="firstname", type="string", length=255)
     */
    private $firstname;

    /**
     * @var string
     *
     * @ORM\Column(name="lastname", type="string", length=255)
     */
    private $lastname;

    /**
     * @var int
     *
     * @ORM\Column(name="age", type="smallint")
     */
    private $age;

    /**
     * @var string
     *
     * @ORM\Column(name="level", type="string", length=255)
     */
    private $level;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="birthdate", type="datetime")
     */
    private $birthdate;

    /**
     * @var string
     *
     * @ORM\Column(name="birthplace", type="string", length=255)
     */
    private $birthplace;

    /**
     * @var bool
     *
     * @ORM\Column(name="is_complete", type="boolean")
     */
    private $isComplete;

    /**
     * @var string
     *
     * @ORM\Column(name="diploma_status", type="string", length=255)
     */
    private $diplomaStatus;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }



    /**
     * Set firstname
     *
     * @param string $firstname
     *
     * @return Profile
     */
    public function setFirstname($firstname)
    {
        $this->firstname = $firstname;

        return $this;
    }

    /**
     * Get firstname
     *
     * @return string
     */
    public function getFirstname()
    {
        return $this->firstname;
    }

    /**
     * Set lastname
     *
     * @param string $lastname
     *
     * @return Profile
     */
    public function setLastname($lastname)
    {
        $this->lastname = $lastname;

        return $this;
    }

    /**
     * Get lastname
     *
     * @return string
     */
    public function getLastname()
    {
        return $this->lastname;
    }

    /**
     * Set age
     *
     * @param integer $age
     *
     * @return Profile
     */
    public function setAge($age)
    {
        $this->age = $age;

        return $this;
    }

    /**
     * Get age
     *
     * @return int
     */
    public function getAge()
    {
        return $this->age;
    }

    /**
     * Set level
     *
     * @param string $level
     *
     * @return Profile
     */
    public function setLevel($level)
    {
        $this->level = $level;

        return $this;
    }

    /**
     * Get level
     *
     * @return string
     */
    public function getLevel()
    {
        return $this->level;
    }

    /**
     * Set birthdate
     *
     * @param \DateTime $birthdate
     *
     * @return Profile
     */
    public function setBirthdate($birthdate)
    {
        $this->birthdate = $birthdate;

        return $this;
    }

    /**
     * Get birthdate
     *
     * @return \DateTime
     */
    public function getBirthdate()
    {
        return $this->birthdate;
    }

    /**
     * Set birthplace
     *
     * @param string $birthplace
     *
     * @return Profile
     */
    public function setBirthplace($birthplace)
    {
        $this->birthplace = $birthplace;

        return $this;
    }

    /**
     * Get birthplace
     *
     * @return string
     */
    public function getBirthplace()
    {
        return $this->birthplace;
    }

    /**
     * Set isComplete
     *
     * @param boolean $isComplete
     *
     * @return Profile
     */
    public function setIsComplete($isComplete)
    {
        $this->isComplete = $isComplete;

        return $this;
    }

    /**
     * Get isComplete
     *
     * @return bool
     */
    public function getIsComplete()
    {
        return $this->isComplete;
    }

    /**
     * Set diplomaStatus
     *
     * @param string $diplomaStatus
     *
     * @return Profile
     */
    public function setDiplomaStatus($diplomaStatus)
    {
        $this->diplomaStatus = $diplomaStatus;

        return $this;
    }

    /**
     * Get diplomaStatus
     *
     * @return string
     */
    public function getDiplomaStatus()
    {
        return $this->diplomaStatus;
    }

    /**
     * Set user
     *
     * @param \StudentBundle\Entity\User $user
     *
     * @return Profile
     */
    public function setUser(\StudentBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \StudentBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set diploma
     *
     * @param \StudentBundle\Entity\Diploma $diploma
     *
     * @return Profile
     */
    public function setDiploma(\StudentBundle\Entity\Diploma $diploma = null)
    {
        $this->diploma = $diploma;

        return $this;
    }

    /**
     * Get diploma
     *
     * @return \StudentBundle\Entity\Diploma
     */
    public function getDiploma()
    {
        return $this->diploma;
    }
}
