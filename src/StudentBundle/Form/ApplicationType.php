<?php

namespace StudentBundle\Form;

use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ApplicationType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
        //Je rajoute un champ diplôme basé sur l'entité en bdd
            ->add('diplomas', EntityType::class, array(
                //j'utilise l'entité diplôme
                'class' => 'StudentBundle:Diploma',
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('d')
                        ->where('d.isOnline = :online')
                        ->setParameter('online', 1);
                },
                //je veux que les libellés utilisent le champ name de l'entité diplôme
                'choice_label' => 'name',
                //j'utilise des boutons radios car choix unique
                'multiple' => false,
                'expanded' => true,
                'mapped' => false
            ));

    }
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'StudentBundle\Entity\Diploma'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'studentbundle_apply';
    }


}
