<?php

namespace StudentBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\BirthdayType;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class ProfileType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('firstname', TextType::class, [
                'label' => 'Prénom'
            ])
            ->add('lastname', TextType::class, [
                'label' => 'Nom'
            ])
            ->add('age')
            ->add('level', TextType::class, [
                'label' => 'Niveau'
            ])
            ->add('birthdate', BirthdayType::class, array(
                'placeholder' => array(
                    'year' => 'Année', 'month' => 'Mois', 'day' => 'Jour',
                ),
                'label' => 'Date de naissance'
            ))
            ->add('birthplace', TextType::class, [
                'label' => 'Lieu de naissance'
            ]);
            // ->add('isComplete', HiddenType::class, ['data' => 1]);
            // ->add('diplomaStatus', HiddenType::class, ['data' => 'pending']);
            // ->add('user', HiddenType::class, ['data' => 1]);
            // ->add('diploma');
    }
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'StudentBundle\Entity\Profile'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'studentbundle_profile';
    }


}
