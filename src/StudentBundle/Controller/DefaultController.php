<?php

namespace StudentBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\VarDumper\VarDumper;

class DefaultController extends Controller
{
    public function indexAction()
    {

        $profile = $this->getUser()->getProfile();
        $diploma = null;
        if (!is_null($profile)) {
            $diploma = $this->getUser()->getProfile()->getDiploma();
        }

        return $this->render('Accueil/index.html.twig', array(
            'profile' => $profile,
            'diploma' => $diploma
        ));

    }
}
