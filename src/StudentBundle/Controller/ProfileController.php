<?php

namespace StudentBundle\Controller;

use StudentBundle\Entity\Profile;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use \Swift_Mailer;

/**
 * Profile controller.
 *
 */
class ProfileController extends Controller
{
    /**
     * Lists all profile entities.
     *
     */

    /**
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $profilesPending = $em->getRepository('StudentBundle:Profile')->findBy(
            ['diplomaStatus' => 'pending']
        );

        $profilesValidated = $em->getRepository('StudentBundle:Profile')->findBy(
            ['diplomaStatus' => 'accepted']
        );

        $profilesRejected = $em->getRepository('StudentBundle:Profile')->findBy(
            ['diplomaStatus' => 'rejected']
        );
        return $this->render('profile/index.html.twig', array(
            'profilesPending' => $profilesPending,
            'profilesValidated' => $profilesValidated,
            'profilesRejected' => $profilesRejected
        ));
    }

    /**
     * Creates a new profile entity.
     *
     */
    public function newAction(Request $request)
    {
        $profile = new Profile();
        $form = $this->createForm('StudentBundle\Form\ProfileType', $profile);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $profile->setDiplomaStatus('created');
            $profile->setIsComplete(1);
            $profile->setUser($this->getUser());
            $em->persist($profile);
            $em->flush();

            return $this->redirectToRoute('profile_show', array('id' => $profile->getId()));
        }

        return $this->render('profile/new.html.twig', array(
            'profile' => $profile,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a profile entity.
     *
     */

    public function showAction(Profile $profile)
    {
        $deleteForm = $this->createDeleteForm($profile);

        return $this->render('profile/show.html.twig', array(
            'profile' => $profile,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing profile entity.
     *
     */
    public function editAction(Request $request, Profile $profile)
    {
        $deleteForm = $this->createDeleteForm($profile);
        $editForm = $this->createForm('StudentBundle\Form\ProfileType', $profile);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('profile_edit', array('id' => $profile->getId()));
        }

        return $this->render('profile/edit.html.twig', array(
            'profile' => $profile,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a profile entity.
     *
     */

    /**
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function deleteAction(Request $request, Profile $profile)
    {
        $form = $this->createDeleteForm($profile);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($profile);
            $em->flush();
        }

        return $this->redirectToRoute('profile_index');
    }

    /**
     * Creates a form to delete a profile entity.
     *
     * @param Profile $profile The profile entity
     *
     * @return \Symfony\Component\Form\Form The form
     */

    /**
     * @Security("has_role('ROLE_ADMIN')")
     */
    private function createDeleteForm(Profile $profile)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('profile_delete', array('id' => $profile->getId())))
            ->setMethod('DELETE')
            ->getForm();
    }

    /**
     * Update a profile and accept his apply.
     *
     */

    /**
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function acceptAction(Profile $profile, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        //$profile = $em->getRepository('StudentBundle:Profile')->find($profileId);
        $profile->setDiplomaStatus('accepted');
        $em->persist($profile);
        $em->flush();

        $message = (new \Swift_Message('Candidature'))
            ->setFrom('tr0ll@alwaysdata.net')
            ->setTo($profile->getUser()->getEmail())
            ->setBody(
                $this->renderView(
                    'Emails/accepted.html.twig',
                    array('name' => $profile->getFirstname())
                ),
                'text/html'
            );

        $this->get('mailer')->send($message);

        return $this->redirectToRoute('profile_index');
    }

    /**
     * Update a profile and reject his apply.
     *
     */

    /**
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function rejectAction(Profile $profile, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $profile->setDiplomaStatus('rejected');
        $em->persist($profile);
        $em->flush();

        $message = (new \Swift_Message('Candidature'))
            ->setFrom('tr0ll@alwaysdata.net')
            ->setTo($profile->getUser()->getEmail())
            ->setBody(
                $this->renderView(
                    'Emails/rejected.html.twig',
                    array('name' => $profile->getFirstname())
                ),
                'text/html'
            );

        $this->get('mailer')->send($message);

        return $this->redirectToRoute('profile_index');
    }
}
