<?php

namespace StudentBundle\Controller;

use StudentBundle\Entity\Diploma;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

/**
 * Diploma controller.
 *
 */
class DiplomaController extends Controller
{
    /**
     * Lists all diploma entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $diplomas = $em->getRepository('StudentBundle:Diploma')->findAll();

        return $this->render('diploma/index.html.twig', array(
            'diplomas' => $diplomas,
        ));
    }

    /**
     * Creates a new diploma entity.
     *
     */

    /**
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function newAction(Request $request)
    {
        $diploma = new Diploma();
        $form = $this->createForm('StudentBundle\Form\DiplomaType', $diploma);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($diploma);
            $em->flush();

            return $this->redirectToRoute('diploma_show', array('id' => $diploma->getId()));
        }

        return $this->render('diploma/new.html.twig', array(
            'diploma' => $diploma,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a diploma entity.
     *
     */

    /**
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function showAction(Diploma $diploma)
    {
        $deleteForm = $this->createDeleteForm($diploma);

        return $this->render('diploma/show.html.twig', array(
            'diploma' => $diploma,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing diploma entity.
     *
     */

    /**
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function editAction(Request $request, Diploma $diploma)
    {
        $deleteForm = $this->createDeleteForm($diploma);
        $editForm = $this->createForm('StudentBundle\Form\DiplomaType', $diploma);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('diploma_edit', array('id' => $diploma->getId()));
        }

        return $this->render('diploma/edit.html.twig', array(
            'diploma' => $diploma,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a diploma entity.
     *
     */

    /**
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function deleteAction(Request $request, Diploma $diploma)
    {
        $form = $this->createDeleteForm($diploma);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($diploma);
            $em->flush();
        }

        return $this->redirectToRoute('diploma_index');
    }

    /**
     * Creates a form to delete a diploma entity.
     *
     * @param Diploma $diploma The diploma entity
     *
     * @return \Symfony\Component\Form\Form The form
     */

    /**
     * @Security("has_role('ROLE_ADMIN')")
     */
    private function createDeleteForm(Diploma $diploma)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('diploma_delete', array('id' => $diploma->getId())))
            ->setMethod('DELETE')
            ->getForm();
    }

    /**
     * Show & Apply to a diploma entity.
     *
     */
    public function applyAction()
    {
        $em = $this->getDoctrine()->getManager();

        $diplomas = $em->getRepository('StudentBundle:Diploma')->findBy(
            ['isOnline' => 1]
        );

        return $this->render('diploma/apply.html.twig', array(
            'diplomas' => $diplomas,
        ));
    }

    /**
     * Update a profile entity with diploma.
     *
     */
    public function updateprofileAction(Diploma $diploma, Request $request)
    {
        $profileId = $this->getUser()->getProfile()->getId();
        $em = $this->getDoctrine()->getManager();
        $profile = $em->getRepository('StudentBundle:Profile')->find($profileId);
        $profile->setDiploma($diploma);
        $profile->setDiplomaStatus('pending');
        $em->persist($profile);
        $em->flush();

        return $this->redirectToRoute('student_dashboard');
    }

}
