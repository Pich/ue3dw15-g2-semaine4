# Rapport - Groupe2

## Accès

Dépôt git : https://framagit.org/Pich/ue3dw15-g2-semaine4.git

Site en ligne : http://tr0ll.alwaysdata.net/

Accès admin au site : 

- Login : Admin
- Password : ZEkvQpBuWi4Vb7E

## Se poser les bonnes questions

Pour faciliter la mise en œuvre du projet et son développement, nous allons procéder selon les étapes décrites dans le cours.

### Question 1: Quel est le but de cette application ?

Le but de cette application est de créer un portail d'inscription en ligne à des formations pour des étudiants. Ce portail doit permettre à des étudiants, une fois authentifiés, de candidater à une formation parmi celles proposées, en **un seul clic**. Le portail doit avoir une interface d'administration où l'administrateur doit pouvoir éditer, supprimer, afficher des formations ainsi que valider les candidatures en donnant un avis favorable ou non à celles-ci. Cette dernière action doit générer un envoi de mail à l'étudiant pour l'informer de la décision.

### Question 2: Qui sont les acteurs ?

- Les futurs étudiants
- Les d'administrateurs

### Question 3: Quelles sont les cas d'utilisation possibles ?

| Code | **Acteur**       | Action                                                       |
| ---- | ---------------- | ------------------------------------------------------------ |
| 1    | Etudiants, Admin | Créer un compte                                              |
| 2    | Etudiants, Admin | Afficher une page pour compléter et éditer son profil        |
| 3    | Etudiants, Admin | Afficher les formations                                      |
| 4    | Etudiants, Admin | Postuler à **une** formation                                 |
| 5    | Etudiants, Admin | Afficher une page qui récapitule ses informations de compte et de formation |
| 6    | Admin            | Afficher une page de profil lui permettant d'éditer son compte |
| 7    | Admin            | Afficher une page pour créer, éditer, gérer les formations   |
| 8    | Admin            | Afficher une page pour gérer les candidatures, avec envoi de mail à l'étudiant |

## Mise en place de notre projet

Le mise en place du projet Symfony s'est faite conformément à ce qui a été réalisé en *semaine 3*. Un dépôt [Framagit](https://framagit.org/Pich/ue3dw15-g2-semaine4) a été créé pour faciliter la collaboration et les déploiemente en production.

### Création du Bundle Student

Une fois le projet installé et déployé sur le dépôt GIT, j'ai créé le premier *Bundle* : **StudentBundle** pour l'application :

````shell
php bin/console generate:bundle
````

## Installation de FOSUserBundle: Gestion des utilisateurs

J'ai par la suite procédé à la mise en place du *FOSUserBundle*, afin de se faciliter la gestions des utilisateurs. A cet effet, j'ai suivi de façon linéaire la [documentation fournie](https://symfony.com/doc/master/bundles/FOSUserBundle/index.html).

## Gestion des entités et CRUD

Dans cette étape, je crée les entités qui seront utilisées dans notre application. La première étape de ce travail a consisté à conceptualiser notre base de données :

![schemaBDD](D:\Code\Laragon\ue3dw15semaine4\schemaBDD.png)

Le seconde étape a été de générer les entités avec *Doctrine* :

````
php bin/console doctrine:generate:entity
````

Deux entités ont été créées : *profile* et *diploma*. *Profile* pour le dossier des étudiants, et *Diploma* pour les formations.

Lors de la troisième étape, j'ai établi les relations entre les tables puis mis à jour le schéma de la base de données :

````php
<?php
/** @Entity */
class Diploma
{
	// ...
	/**
     * One Diploma has Many Profiles.
     * @ORM\OneToMany(targetEntity="Profile", mappedBy="diploma")
     */
    private $profiles;
    
    public function __construct()
    {
        $this->profiles = new ArrayCollection();
    }
 }
````

````php
<?php
/** @Entity */
class Profile
{
	/**
     * One Profile has One User.
     * @ORM\OneToOne(targetEntity="User", inversedBy="profile")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    private $user;

    /**
     * Many Profiles have One Diploma.
     * @ORM\ManyToOne(targetEntity="Diploma", inversedBy="profiles")
     * @ORM\JoinColumn(name="diploma_id", referencedColumnName="id")
     */
    private $diploma;
}
````

````php
 <?php
/** @Entity */
class User
{	
 	/**
     * One User has One Profile.
     * @ORM\OneToOne(targetEntity="Profile", mappedBy="user")
     */
    private $profile;
}
````

La commande ` php bin/console doctrine schema:update --force` permet de mettre à jour le schéma de la base de données avec ces informations.

Le dernière étape consiste à générer le CRUD pour ces entités. La commande ` php bin/console doctrine:generate:crud`  a été exécutée pour chaque entité.

A la fin de ces étapes, notre application dispose d'une structure MVC complète, à savoir, des *vues*, un *routeur*, des *controllers* qui y sont associés, ainsi que des *entités* pour chaque table de la base de données.

## Routeur et vues

Les vues et le routeur ont été générées automatiquement par les différentes étapes réalisées ci-dessus. Cependant, il convient de les modifier pour les adapter au projet et aux fonctionnalités attendues listées dans [le tableau](#Question 3: Quelles sont les cas d'utilisation possibles ?). 

Par exemple, pour la fonctionnalité n°8 : *Afficher une page pour gérer les candidatures*, nous avons ajouté au routeur : 

````yaml
profile_accepted:
    path:     /{id}/accept
    defaults: { _controller: "StudentBundle:Profile:accept" }
    methods:  GET

profile_rejected:
    path:     /{id}/reject
    defaults: { _controller: "StudentBundle:Profile:reject" }
    methods:  GET
````

et créer les *controller*s associés :

````php
	/**
     * Update a profile and accept his application.
     *
     */

    /**
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function acceptAction(Profile $profile, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $profile->setDiplomaStatus('accepted');
        $em->persist($profile);
        $em->flush();

        $message = (new \Swift_Message('Candidature'))
            ->setFrom('tr0ll@alwaysdata.net')
            ->setTo($profile->getUser()->getEmail())
            ->setBody(
                $this->renderView(
                    'Emails/accepted.html.twig',
                    array('name' => $profile->getFirstname())
                ),
                'text/html'
            );

        $this->get('mailer')->send($message);

        return $this->redirectToRoute('profile_index');
    }

    /**
     * Update a profile and reject his application.
     *
     */

    /**
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function rejectAction(Profile $profile, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $profile->setDiplomaStatus('rejected');
        $em->persist($profile);
        $em->flush();

        $message = (new \Swift_Message('Candidature'))
            ->setFrom('tr0ll@alwaysdata.net')
            ->setTo($profile->getUser()->getEmail())
            ->setBody(
                $this->renderView(
                    'Emails/rejected.html.twig',
                    array('name' => $profile->getFirstname())
                ),
                'text/html'
            );

        $this->get('mailer')->send($message);

        return $this->redirectToRoute('profile_index');
    }
````

L'envoi de mail défini dans la fonctionnalité n°8 a été mis en place avec le *[Bundle SwiftMailer](https://symfony.com/doc/3.4/email.html)* livré de base avec *Symfony.*

La majeure partie du travail réalisé sur ce projet à consister à modifier et créer les vues, routeur, et *controllers*, comme dans l'exemple donné, pour les adapter aux fonctionnalités demandées.

## Mise en page et style de l'application

Pour une question de temps, j'ai décidé d'utiliser le *framework CSS Bootstrap v4* pour la mise en page et le look de l'application. Sa mise en place s'est faite par *CDN* :

````html
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
````

````html
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
````

De plus, j'ai utilisé la mise en style automatique des formulaires proposée par Symfony, laquelle implémente Bootstrap 4 à l'aide du moteur de template *Twig* :

````yaml
# Twig Configuration
twig:
    form_themes: ['bootstrap_4_layout.html.twig']
````

## Mise en production

J'ai, conformément aux consignes, utilisé l'offre gratuite d'hébergement mutualisé d'*alwaysdata* pour la mise en production de l'application. Cette étape n'a posé aucun problème particulier. Après création du compte et configuration de celui-ci via l'interface d'administration, le simple jeu de commandes : 

````sh
git clone https://framagit.org/Pich/ue3dw15-g2-semaine4.git
composer update
php bin/console doctrine:schema:update --force
````

m'a permis de mettre en ligne l'application.

A noter que, lors de la commande d'installation des dépendances `composer update`, Symfony a détecté qu'il s'agissait d'un nouveau déploiement. Un script a lancé l'assistant de création du fichier de configuration `app/config/parameters.yml`, dans la mesure où celui-ci ne fait pas partie des fichiers "versionnés" avec GIT.  

## Conclusion personnelle - Vincent Pichot

Le travail de cette semaine a été très instructif, en particulier car je n'ai pas l'habitude d'utiliser Symfony. L'entreprise dans laquelle j'effectue mon contrat de professionnalisation a fait le choix d'utiliser son concurrent *Laravel*. Bien que ces deux *Framework* aient des similitudes, l'emploi de *Doctrine* en tant qu'ORM a été pour moi une nouveauté. 

J'aurais aimé, avec plus de temps, améliorer l'application en y ajoutant des messages de *feedback*s pour les différentes actions de formulaire. De même, j'aurais aimé pouvoir approfondir ma prise en main de Doctrine.

Du point de vue du fonctionnement du groupe, je regrette d'avoir travaillé seul. Le seul membre qui a répondu présent était **Idriss Driouch**. Bien qu'il se soit manifesté tardivement (jeudi 01/10), je lui avait proposé de travailler sur le templating et l'intégration, ce qu'il avait accepté. Néanmoins, à ce jour (dimanche 04/11, 17h), il n'a toujours pas soumis son travail, c'est pourquoi j'ai réalisé moi-même cette étape. 